import ast
import json
import re
import os

# from getpass import getpass


from pyArango.connection import *
from typing import List  # , Union, Dict, Any, Optional, cast

from analiseTextoNormas import ArquiteturaDados


class DatabaseInterface:
    def __init__(self):
        f = open('connection.json')
        connection_setup = json.load(f)
        user = connection_setup['users'][0]['username']
        password = connection_setup['users'][0]['passwordtext']
        database_name = connection_setup['database']
        conn = Connection(username=user, password=password)
        db = conn[database_name]

        self.__norma_collection = db.collections['Norma']

        self.__edge_interno = db.collections['Interno']
        self.__edge_externo = db.collections['Externo']

    @staticmethod
    def __limpa_urn(urn: str) -> str:
        urn = re.sub(':', '.', urn)
        urn = re.sub('urn.lex.', '', urn)
        urn = re.sub(';', '.', urn)
        urn = re.sub('!', '.', urn)
        urn = re.sub('@', '__', urn)
        return urn

    def __add_edge_tipo(self, edges_list: List[dict], tipo: str):
        """

        :param edges_list:
        :param tipo:
        :return:
        """
        print('adicionando arestas do tipo...', tipo)
        for edge in edges_list:
            del edge['tipoJson']
            print(edge['_to'])
            edge['_from'] = self.__limpa_urn(edge['_from'])
            edge['_to'] = self.__limpa_urn(edge['_to'])
            key = edge['_from'] + '@' + edge['_to']
            print(edge['_to'])

            if (tipo == 'evolucao') and (key in self.__norma_collection):
                for aresta in edges_list:
                    # @todo resolver isso
                    self.update_edge(key, aresta)

            else:

                # caso não exista,  acrescentar

                if tipo == 'interno':
                    doc = self.__edge_interno.createDocument()

                # se não for interno, automaticamente ele entende como externo
                else:
                    doc = self.__edge_externo.createDocument()

                for chave, valor in edge.items():
                    if chave == '_from' or chave == '_to':
                        if valor == '':
                            valor = 'relacionamentos_nao_identificados'
                            doc[chave] = 'Norma/' + valor
                        else:
                            doc[chave] = 'Norma/' + valor
                    else:
                        doc[chave] = valor

                # doc._key = key
                print(doc)
                doc.save()
                del doc

    @staticmethod
    def __separa_nodes(mix_edges_list: List[dict]):
        nodes = []
        print(mix_edges_list)
        # getpass('pressione enter')
        for value in mix_edges_list:
            print(value)
            print('value["tipoJson"]', value['tipoJson'])
            # getpass('pressione enter')
            if value['tipoJson'] == 'node':
                nodes.append(value)
        return nodes

    @staticmethod
    def __separa_edges(mix_edges_list: List[dict], tipo):
        tipologia = ArquiteturaDados.type_edges
        arestas = []
        for value in mix_edges_list:
            if value['tipoJson'] == 'aresta' and value['tipo'] in tipologia[tipo]:
                arestas.append(value)
        return arestas

    def batch_edges_add(self, mix_edges_list: List[dict], tipo='geral'):
        """

        :param mix_edges_list:
        :param tipo:
        :return:
        """
        # tipologias = ArquiteturaDados.type_edges
        # print('edge', mix_edges_list )
        # print(';;;;;;;;;;;;;;;;entrou no batch_edges')

        # getpass('pressione enter')
        if tipo == 'geral':
            edges: List[dict] = self.__separa_edges(mix_edges_list, 'externo')
            if edges:
                self.__add_edge_tipo(edges, 'externo')

            edges = self.__separa_edges(mix_edges_list, 'interno')
            if edges:
                self.__add_edge_tipo(edges, 'interno')

        elif tipo == 'externo':
            self.__add_edge_tipo(mix_edges_list, 'externo')

        elif tipo == 'interno':
            self.__add_edge_tipo(mix_edges_list, 'interno')

    def batch_nodes_add(self, nodes_list: List[dict]):
        """

        :param nodes_list:
        :return:
        """
        print('entrou no batch_nodes_add ')
        # print(nodes_list)

        for node in nodes_list:
            # print('entrou no node lists')
            node['urnParticula'] = self.__limpa_urn(node['urnParticula'])
            # print('node[urnParticula]', node['urnParticula'] )
            if node['urnParticula'] in self.__norma_collection:
                doc = self.__norma_collection[node['urnParticula']]
                print('nó ' + node['urnParticula'] + ' atualizado!')
            else:
                doc = self.__norma_collection.createDocument()
                doc._key = node['urnParticula']
            for chave, valor in node.items():
                if chave == 'tipoJson':
                    continue
                doc[chave] = valor

            doc.save()
            print('nó ' + node['urnParticula'] + ' criado!')

            del doc

    def fit_directory(self, directory: str):
        """

        :param directory:
        :return:
        """
        counter_sucess: int = 0
        if not os.path.isdir(directory):
            return False

        for file in os.listdir(directory):

            print('...', counter_sucess, ' arquivos populados no banco')
            counter_sucess += 1
            if file.endswith(".nbex"):
                print('... convertendo o arquivo', file)
                file = directory + '/' + file
                with open(file, encoding='utf-8', errors='replace') as f:
                    try:
                        mix_edges_list: List[dict] = ast.literal_eval(f.read())
                    except (TypeError, ValueError):
                        print('não foi possível ler o arquivo :', file)
                        continue
                    f.close()

                # adicionando os nos
                nodes = self.__separa_nodes(mix_edges_list)
                self.batch_nodes_add(nodes)

                # separando as arestas

                # arestas externas
                arestas_externas: List[dict] = self.__separa_edges(mix_edges_list, 'externo')
                self.batch_edges_add(arestas_externas, 'externo')

                # arestas internas
                arestas_externas: List[dict] = self.__separa_edges(mix_edges_list, 'interno')
                self.batch_edges_add(arestas_externas, 'interno')

    def update_node(self, key: str, node: dict):
        """

        :param key:
        :param node:
        :return:
        """

        if key in self.__norma_collection:
            doc = self.__norma_collection[key]
            for chave, valor in node.items():
                doc[chave] = valor
            doc.save()
            return True
        else:
            return False

    def update_edge(self, key: str, edge: dict, tipo: str):
        """

        :param key:
        :param edge:
        :param tipo:
        :return:
        """
        # se o no é do tipo evolução e o nó que esta sendo alterado não existe no banco então, a verdade estamos
        # diante do evento de instituição de uma nova recação e não de evolucao da redação
        if edge['_to'] not in self.__norma_collection and edge['tipo'] == 'evolucao':
            pass
        else:
            if tipo == 'interno':
                doc = self.__edge_interno.createDocument()
            else:
                doc = self.__edge_externo.createDocument()

            for chave, valor in edge.items():
                if chave == 'tipoJson':
                    continue
                doc[chave] = valor
                doc._key = edge['_from'] + '@' + edge['_to']
                doc.save()
            del doc
