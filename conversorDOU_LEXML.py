import os
#from getpass import getpass
from typing import Union

import requests
import json
import zipfile
import re
import xmltodict

from datetime import date
from lxml import etree
from unidecode import unidecode

from analiseTextoNormas import ArquiteturaDados
from analiseTextoNormas import TratamentoDasNormas
from conversorHTM_LEXML import CodexHTMtoDOCX
from datetime import datetime

#java -jar lexml-parser-projeto-lei-1.12.3-SNAPSHOT-onejar.jar parse -t 'decreto' -n 280 ...
# ... -m application/vnd.openxmlformats-officedocument.wordprocessingml.document --ano 2021 -i exemplo_infralegal.docx


class GetINlabs:

    def __init__(self):

        f_connect = open('connection.json')
        connection_setup = json.load(f_connect)
        f_connect.close()

        self.__tratanormas = TratamentoDasNormas(False)
        self.__model = ArquiteturaDados.node_model
        self.__web_scrapping = CodexHTMtoDOCX('decreto', '/xmldou_docx/')
        self.__login = connection_setup['dou'][0]['login']
        self.__senha = connection_setup['dou'][0]['senha']

        self.__tipo_dou = "DO1 DO1E"  # Seções separadas por espaço
        # Opções DO1 DO2 DO3 DO1E DO2E DO3E

        url_login = "https://inlabs.in.gov.br/logar.php"
        self.__url_download = "https://inlabs.in.gov.br/index.php?p="

        payload = {"email": self.__login, "password": self.__senha}
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        }
        self.__s = requests.Session()
        response = self.__s.request("POST", url_login, data=payload, headers=headers, verify=False)

        if self.__s.cookies.get('inlabs_session_cookie'):
            self.__cookie = self.__s.cookies.get('inlabs_session_cookie')
        else:
            print("Falha ao obter cookie. Verifique suas credenciais")
            exit(37)

        # Montagem da URL:
        ano = date.today().strftime("%Y")
        mes = date.today().strftime("%m")
        dia = date.today().strftime("%d")
        self.__data_completa = ano + "-" + mes + "-" + dia
        self.validate_xmldou = etree.XMLSchema(file="xmldou_schema.xsd")

    def __unzip(self, zip_filename: str, path_to_extract: str) -> bool:
        try:
            with zipfile.ZipFile(zip_filename, 'r') as zip_ref:
                zip_ref.extractall(path_to_extract)
            return True
        except:
            return False

    def __xmldou_getparams_to_parser(self):
        pass

    def __xmldou_to_lexml(self, xmldou_file):
        is_valid = self.validate_xmldou.validate(xmldou_file)
        pass

    def __set_outros_particula(self):
        pass

    def __save_nbex(self):
        pass

    def __trata_conteudo(self, content: str, epigrafe: str, ordem_normativa: str) -> str:
        """
        Limpa o conteudo constante no atributo <Texto> do XML DOU
        :param content: str conteudo constante no atributo <Texto> do XML DOU
        :param epigrafe: str epigrafe a ser inserida na norma caso ela não exista
        :param ordem_normativa:  str tipologia: Portaria, Instrução Normativa, etc...
        :return: str com o texto limpo, pronto para ser transformado em docx e lexml
        """
        content = re.sub(r'<p class=".{3}titulo">.*?</p>', '', content)
        content = re.sub(r'<p></p>', '', content)

        ordem_normativa = ordem_normativa.lower().strip()
        if not re.search(r'<p class="identifica">.+?</p>', content):
            epigrafe_ajustada = '<p class="identifica">' + epigrafe + '</p>'
            content = epigrafe_ajustada + content

        epigrafe_no_html = re.search(r'<p class="identifica">.+?</p>', content).group(0)
        epigrafe_no_html = re.sub(r'<.*?>','', epigrafe_no_html).lower()

        if epigrafe_no_html not in ArquiteturaDados.normas_compostas:
            epigrafe_no_html = '<p class="identifica">' + ' Decreto ' +  epigrafe_no_html + '</p>'
            content = re.sub(r'<p class="identifica">.+?</p>',epigrafe_no_html,content,1 )

        return content

    def __urn_solver(self, metadados_norma):
        """
          metadados_norma = {
                    'numero': numero_norma,
                    'ano': dt.year,
                    'tipo_norma': ordem_normativa,
                    'conteudo': conteudo_tratado,
                    'orgao_origem': orgao_origem,
                    'data_assinatura' : data_assinatura
                }
        :param metadados_norma:
        :return:
        """
        # ministerio.de.minas.e.energia;agencia.nacional.do.petroleo,.gas.natural.e.biocombustiveis
        # URN="urn:lex:br:federal:decreto:2004;5243@data.evento;leitura;2021-01-20t10.32"/

        urn_base = 'urn:lex:br'

        orgao_origem = metadados_norma['orgao_origem'].replace('/', '..').replace(' ', '.')
        orgao_origem = orgao_origem.replace('.da.', '.').replace('.de.', '.').replace('.e.', '.')
        if len(orgao_origem) > 40:
            orgao_origem = re.sub("[a-zàáéíóúãẽĩõũâêîçôû]", '', orgao_origem)
            orgao_origem = re.sub(r"\.([^\.])", r"\1", orgao_origem)
        ordem_normativa = metadados_norma['tipo_norma']


        for norma_separada, norma_underline in ArquiteturaDados.normas_compostas.items():
                ordem_normativa = ordem_normativa.replace(norma_separada, norma_underline, 1)

        urn_resultante = urn_base + ':' + orgao_origem + ':' + ordem_normativa + ':' + metadados_norma['ano'] + ':' + \
                         metadados_norma['numero']

        return urn_resultante

    def __epigrafe_solver(self, metadados_norma: dict) -> str:
        """
        Cria uma string a estilo epigrafe, a partir do dicionário passado como parâmetro
        metadados_norma = {
                        'numero': str(numero_norma),
                        'ano': str(dt.year),
                        'tipo_norma': str(ordem_normativa),
                        #'tipo_norma_parser': str(ordem_normativa_parser),
                        'ementa': ementa,
                        'epigrafe': epigrafe,
                        'conteudo': conteudo_tratado,
                        'orgao_origem': orgao_origem,
                        'data_assinatura' : data_assinatura, #'%Y-%m-%d'
                        'is_norma_legal' : is_norma_legal
                    }

        :param metadados_norma:
        :return: str tipo : Portaria Ministério da Economia 250
        """

        #dt = datetime.strptime(data_assinatura, '%Y-%m-%d')
        #dt = datetime.strptime(data_assinatura, '%Y-%m-%d')
        data_assinatura_expressa =  self.__tratanormas.data_express_from_pattern(metadados_norma['data_assinatura']) # dd/mm/yyyy



        if not metadados_norma['numero']:
            complemento_numero = 'sem número'
            metadados_norma['numero'] = ' '
        else:
            complemento_numero = ' nº '

        if not metadados_norma['orgao_origem']:
            metadados_norma['orgao_origem'] = ' '

        epigrafe = metadados_norma['tipo_norma'] + ' ' + metadados_norma['orgao_origem'] + complemento_numero + \
                   metadados_norma['numero'] + ', de ' + data_assinatura_expressa
        return epigrafe


    def __preambulo_solver(self, metadados_norma):
        return metadados_norma['conteudo']

    def __monta_lexml_fake(self, metadados_norma, orgao_origem):
        """
         metadados_norma = {'numero': numero_norma, 'ano': dt.year, 'tipo_norma': ordem_normativa,
                                                                                           'conteudo': conteudo_tratado}
        :param metadados_norma:
        :param orgao_origem:
        :return:
        """
        lexml_model: dict = xmltodict.parse(ArquiteturaDados.lexml_model)
        lexml_model['LexML']['Metadado']['Identificacao']['@URN'] = self.__urn_solver(metadados_norma)
        lexml_model['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Ementa']['#text'] = metadados_norma['ementa']
        lexml_model['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Epigrafe']['#text'] = self.__epigrafe_solver(metadados_norma)
        lexml_model['LexML']['ProjetoNorma']['Norma']['ParteInicial']['Preambulo']['#text'] = self.__preambulo_solver(metadados_norma)

        return lexml_model

    def __save_xml(self, lexml_final: str, metadados_norma: dict, diretorio_destino: str):
        """

        :param lexml_final:
        :param metadados_norma:
        :param diretorio_destino:
        :return:
        """
        filename = diretorio_destino+'/'+ metadados_norma['tipo_norma']+'_'+ metadados_norma['numero']+'_' + \
                   metadados_norma['ano'] + '_lexml'  +'.xml'

        if not os.path.isdir(diretorio_destino):
            os.mkdir(diretorio_destino)


        try:
            with open(filename, 'w') as f:
                f.write(str(lexml_final))
            f.close()
        except:
            print('não foi possível converter ou salvar o arquivo', filename)

    def __get_data_expressa_from_epigrafe(self, epigrafe: str) -> Union[str, bool]:
        """
        Retorna data por extenso a partir da epigrafe
        :param epigrafe:
        :return:  Str ou Bool
        """
        meses = ArquiteturaDados.meses
        epigrafe = epigrafe.lower().strip()

        print('epigrafe',epigrafe)
        """
            meses: dict = {"janeiro": "01", "fevereiro": "02", "marco": "03", "abril": "04", "maio": "05", "junho": "06",
                   "julho": "07", "agosto": "08", "setembro": "09", "outubro": "10", "novembro": "11",
                   "dezembro": "12"}
        """
        lst_epigrafe  = epigrafe.split(' de ')
        print ('lst_epigrafe', lst_epigrafe)
        for idx, val in enumerate(lst_epigrafe):
            if idx == 0:
                continue
            if val in meses and idx < len(lst_epigrafe):
                data_expressa = lst_epigrafe[idx-1] + ' de ' + val + ' de ' + lst_epigrafe[idx+1]
                return data_expressa
            else:
                continue
        return False

    def get_zip_dou1(self, subdirectory='.'):
        for dou_secao in self.__tipo_dou.split(' '):

            print("Aguarde Download do caderno"+ dou_secao + '...')
            url_arquivo = self.__url_download + self.__data_completa + "&dl=" + self.__data_completa + "-" + dou_secao \
                          + ".zip"
            cabecalho_arquivo = {'Cookie': 'inlabs_session_cookie=' + self.__cookie, 'origem': '736372697074'}
            response_arquivo = self.__s.request("GET", url_arquivo, headers=cabecalho_arquivo)
            if response_arquivo.status_code == 200:

                if not os.path.isdir(subdirectory):
                    os.mkdir(subdirectory)
                zip_filename = subdirectory + "/" + self.__data_completa + "-" + dou_secao + ".zip"
                with open(zip_filename, "wb") as f:
                    f.write(response_arquivo.content)
                    print("Arquivo %s salvo." % (self.__data_completa + "-" + dou_secao + ".zip"))
                del response_arquivo
                del f
                self.__unzip(zip_filename, subdirectory)
            elif response_arquivo.status_code == 404:
                print("Arquivo não encontrado: %s" % (self.__data_completa + "-" + dou_secao + ".zip"))

    def load_xmldou(self, diretorio_origem='.', diretorio_destino = 'lexml_from_xmldou'):


        for xmldou_file in os.listdir(diretorio_origem):
            xmldou_file = diretorio_origem + "/" + xmldou_file
            print('extraindo dado de ' + str(xmldou_file))
            self.__web_scrapping.file = str(xmldou_file)
            print("xmldou_file", xmldou_file)
            if xmldou_file.endswith('.xml'):
                #print('entrou no if ')
                #getpass('Pressione enter para continuar')

                xml_file = etree.parse(xmldou_file)
                if self.validate_xmldou.validate(xml_file):
                    with open(xmldou_file, encoding='utf-8', errors='replace') as f_xmldou:
                        xmldou_content = xmltodict.parse(f_xmldou.read())
                        f_xmldou.close()
                    ordem_normativa = xmldou_content['xml']['article']['@artType']
                    ordem_normativa = unidecode(ordem_normativa.lower().strip())

                    data_publicacao = xmldou_content['xml']['article']['@pubDate']

                    # montando epigrafe e data de assinatura
                    #print('ordem_normativa', ordem_normativa)
                    #getpass('pressione enter para continuar')
                    if ordem_normativa in ArquiteturaDados.normas_padrao_d9191:
                        is_norma_legal = True
                        #print('///////////////entao a is norma legal é true')
                        #getpass('pressione enter')
                        for norma_separada, norma_underline in ArquiteturaDados.normas_compostas.items():
                            ordem_normativa = ordem_normativa.replace(norma_separada, norma_underline, 1)
                    else:

                        is_norma_legal = False

                        #ordem_normativa = 'decreto'


                    print(xmldou_content)

                    if type(xmldou_content['xml']['article']['body']['Identifica']) == str:
                        epigrafe = xmldou_content['xml']['article']['body']['Identifica']
                        print("xmldou_content['xml']['article']['body']['Identifica']",)

                        data_assinatura_expressa = self.__get_data_expressa_from_epigrafe(epigrafe)
                        if not data_assinatura_expressa:
                            data_assinatura_expressa = self.__tratanormas.data_express_from_pattern(data_publicacao)
                    else:
                        epigrafe = xmldou_content['xml']['article']['@name'] + ' do ' + \
                                   xmldou_content['xml']['article']['@artCategory'] + ' de ' + \
                                   self.__tratanormas.data_express_from_pattern(data_publicacao)

                        data_assinatura_expressa = self.__tratanormas.data_express_from_pattern(data_publicacao)

                    # pegando a numeracao da norma
                    expressao_regular = r'[0-9\.]+'
                    prog = re.compile(expressao_regular, re.I | re.MULTILINE)
                    if type(xmldou_content['xml']['article']['body']['Identifica']) == str:
                        searched = re.search(prog, xmldou_content['xml']['article']['body']['Identifica'])
                    #print("xmldou_content['xml']['article']['body']['Identifica']", xmldou_content['xml']['article']['body']['Identifica'])
                    #getpass('pressione enter')
                    else:
                        pubdate = xmldou_content['xml']['article']['@pubDate']

                        metadados_norma = {
                        'numero': xmldou_content['xml']['article']['@name'],
                        'tipo_norma': xmldou_content['xml']['article']['@artType'],
                        'orgao_origem':  xmldou_content['xml']['article']['@artCategory'],
                        'data_assinatura': pubdate
                        }

                        xmldou_content['xml']['article']['body']['Identifica'] = self.__epigrafe_solver(metadados_norma)

                        #print('epigrafe montada', xmldou_content['xml']['article']['body']['Identifica'])
                        #getpass('pressione enter para prosseguir')

                        searched = re.search(prog, xmldou_content['xml']['article']['body']['Identifica'])

                    if searched:

                        numero_norma: str = searched.group(0)
                        numero_norma = numero_norma.replace('.','')
                    else:
                        numero_norma: str = '0'

                    # outros dados
                    titulo = '@@Não '
                    subtitulo = 'identificado'
                    if type(xmldou_content['xml']['article']['body']['Titulo']) == str:
                        titulo = xmldou_content['xml']['article']['body']['Titulo']
                    if type(xmldou_content['xml']['article']['body']['SubTitulo']) == str:
                        subtitulo = xmldou_content['xml']['article']['body']['SubTitulo']

                    titulo_subtitulo = titulo + " " + subtitulo
                    orgao_origem: str = xmldou_content['xml']['article']['@artCategory']

                    # montando ementa
                    if type(xmldou_content['xml']['article']['body']['Ementa']) == str:
                        ementa: str = xmldou_content['xml']['article']['body']['Ementa']
                    else:
                        ementa: str = titulo_subtitulo

                    # pegar a data formato ISO yyyy-mm-dd ISO 8601
                    print('data_assinatura_expressa', data_assinatura_expressa)
                    data_assinatura: str = self.__tratanormas.get_data_by_expressa(data_assinatura_expressa) #yyyy-mm-dd

                    conteudo_tratado: str = self.__trata_conteudo(
                        xmldou_content['xml']['article']['body']['Texto'],
                        epigrafe,
                        ordem_normativa
                    )
                    docx_derivado = self.__web_scrapping.convert_string_docx(conteudo_tratado)

                    dt = datetime.strptime(data_assinatura, '%Y-%m-%d')

                    metadados_norma = {
                        'numero': str(numero_norma),
                        'ano': str(dt.year),
                        'tipo_norma': str(ordem_normativa),
                        #'tipo_norma_parser': str(ordem_normativa_parser),
                        'ementa': ementa,
                        'epigrafe': epigrafe,
                        'conteudo': conteudo_tratado,
                        'orgao_origem': orgao_origem,
                        'data_assinatura' : data_assinatura,
                        'is_norma_legal' : is_norma_legal,
                        'arquivo_origem' : unidecode(epigrafe).replace(' ','_').lower() + '_em_' + data_assinatura.replace('-','_'),
                        'path_origem' : 'dou://'
                    }




                    #print('metadados_norma', metadados_norma)
                    #getpass('Pressione Enter')
                    if docx_derivado:
                        resultado_lexml = self.__web_scrapping.convert_docx_xml(metadados_norma, epigrafe,
                                                                                ordem_normativa, is_norma_legal)
                        #print('resultado_lexml', resultado_lexml)
                        #getpass('pressione Enter para continuar')

                        if resultado_lexml and not is_norma_legal:
                            print('///////////resultado_lexml and not is_norma_legal',resultado_lexml, is_norma_legal)
                            resultado_lexml_parsed = xmltodict.parse(resultado_lexml)
                            #print("resultado_lexml_parsed['LexML']['Metadado']['Identificacao']['@URN']", resultado_lexml_parsed['LexML']['Metadado']['Identificacao']['@URN'])
                            #getpass('pressione enter para continuar')
                            resultado_lexml_parsed['LexML']['Metadado']['Identificacao']['@URN'] = \
                                self.__urn_solver(metadados_norma)
                            resultado_lexml = xmltodict.unparse(resultado_lexml_parsed)

                            #print('////////////resultado_lexml////////////', resultado_lexml)
                            #getpass('pressione Enter para continuar')

                            lexml_final = self.__web_scrapping.include_pos_parser_datas(resultado_lexml, epigrafe)

                            #print('lexml_final', lexml_final)
                            #getpass('pressione Enter para continuar')
                            self.__save_xml(lexml_final, metadados_norma, diretorio_destino)
                        elif not resultado_lexml and not is_norma_legal:
                            # caso nao tenha sido possível parsear para lexml, montar um lexml 'fake' para aproveitar a urn
                            # e outros dados necessários.

                            lexml_final = self.__monta_lexml_fake(metadados_norma, orgao_origem)
                            #print('lexml_final_fake', lexml_final)
                            #getpass('Presione Enter para continuar')
                            self.__save_xml(xmltodict.unparse(lexml_final), metadados_norma, diretorio_destino)
                        else:
                            self.__save_xml(resultado_lexml, metadados_norma, diretorio_destino)

    def load_xmldou_directory(self):
        pass
