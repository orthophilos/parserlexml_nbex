import sys
from getpass import getpass
from typing import List, Callable
import os
from datetime import datetime


class Observer:
    def __init__(self, savelog=True):
        self.__savelog = savelog
        self.__list_log = []
        self.__log_filename = 'logCodex.txt'
        self.__counter = 0

    @property
    def counter(self):
        return self.__counter

    @property
    def list_log(self):
        return self.__list_log

    def __set_header(self, metadados) -> str:
        header_line = '#'
        for key in metadados.keys():
            header_line = header_line + ';' + str(key)
        return header_line

    def __set_row(self, metadados: dict) -> str:
        """

        :param metadados:
        :return:
        """
        self.__counter += 1
        line = '"' + str(self.__counter) + '"'
        for values in metadados.values():
            line = line + ';' +'"' + str(values) + '"'
        line = line + '\n'
        return line

    def __save_logfile(self, metadados: dict) -> bool:
        """

        :param metadados:
        :return:
        """

        try:
            if os.path.isfile(self.__log_filename):
                f = open(self.__log_filename, 'a')
            else:
                f = open(self.__log_filename, 'w')
                header = self.__set_header(metadados)
                f.write(header)
                f.write('\n')
        except:
            return False
        conteudo_arquivo = self.__set_row(metadados)
        print('conteudo_arquivo' , conteudo_arquivo)
        f.write(conteudo_arquivo)
        f.close()
        print('...log salvo com sucesso...')
        return True

    def register(self, metadados_original: dict, evento: str):
        metadados = metadados_original.copy()
        if metadados['conteudo']:
            del metadados['conteudo']
        metadados['evento'] = evento
        self.__list_log.append(metadados)
        if self.__savelog:
            self.__save_logfile(metadados)
