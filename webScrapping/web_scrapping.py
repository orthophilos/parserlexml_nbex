import os
import sys
import time
import csv
#import xml.etree.ElementTree as ET
from lxml import etree

import pypandoc
import re

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from getpass import getpass

from selenium import webdriver
from selenium.webdriver.support.ui import Select


# from typing import List, Union, Dict, Any, Optional, cast

# @todo tratar save_file

class CodexHTMtoDOCX:

    """
        Modulo de conversão dos arquivos legados do site de legislação,  de .htm para formato lexml.
        Para isso, faz webscrapping, utilizando-se de  uma instância do parser_lexml disponível em :
        'https://legis.senado.gov.br/lexml-parser/parse/static/simulador/simulador.html'.


         Ao instanciar esta classe será necessário informar os seguintes atributos:

         a) tipo_norma - com as seguintes opções: 'decreto', 'decreto legislativo' 'decreto-lei',
         'emenda constitucional','lei', 'lei complementar', 'lei delegada', 'medida provisória'

         b) result_directory - o diretório onde serao gravados os arquivos .xml.

         Utilize o método :

         a) .fit_files: tenta a conversão de todos os arquivos do diretório informado
         b) .fit_directories: converte todos os arquivos que estejam nos diretórios informados em um arquivo .csv

    """
    welcome = """
        PROJETO CODEX . Conversor .htm (Portal da Legislacao) -> .xml (Lexml), utilizando-se de webscrapping 
        do site:  'https://legis.senado.gov.br/lexml-parser/parse/static/simulador/simulador.html'.
    
    """

    normas_tipos = {
        'decreto': 'decreto', 'decreto legislativo': 'decreto.legislativo', 'decreto-lei': 'decreto.lei',
        'emenda constitucional': 'emenda.constitucional',
        'lei': 'lei', 'lei complementar': 'lei.complementar', 'lei delegada': 'lei.delegada',
        'medida provisória': 'medida.provisoria'
    }

    endereco_parser = 'https://legis.senado.gov.br/lexml-parser/parse/static/simulador/simulador.html'

    arquivo_docx_intermediario = 'output.docx'

    meses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro',
             'novembro',
             'dezembro']

    def __init__(self, tipo_norma: str, result_directory: str):
        """
        :param tipo_norma:
        :param result_directory:
        """
        print(self.welcome)
        warning_head_message = '//////////////////////////// ATENÇÃO //////////////////////////////////////'
        if tipo_norma not in self.normas_tipos:
            print("""
                WebScrapping para conversão de  formato .htm para .lexml , utilizando-se do site: \
                'https://legis.senado.gov.br/lexml-parser/parse/static/simulador/simulador.html
            """)
            #tipo_norma_original = tipo_norma
            tipo_norma = 'decreto'
            #sys.exit('ajuste adequadamente o atributo tipo_norma. Execução da aplicação abortada')

        self.__insucesso: list = []
        self.__result_directory: str = result_directory
        self.__tipo_norma: str = tipo_norma
        self.__file: str = ''
        self.__xml_lista_total: list = []

        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.download.folderList', 2)  # custom location
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', '/xmls')
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv')

        # ET.register_namespace('xsi', "http://www.lexml.gov.br/1.0 ../xsd/lexml-br-rigido.xsd")
        #etree.register_namespace('xsi', "http://www.lexml.gov.br/1.0")
        #etree.register_namespace('xsi', "http://www.w3.org/2001/XMLSchema-instance")
        #etree.register_namespace('xlink', "http://www.w3.org/1999/xlink")
        #etree.register_namespace('', "http://www.lexml.gov.br/1.0")
        # xsi: schemaLocation

        self.__browser = webdriver.Firefox()
        self.__browser2 = webdriver.Firefox()
        try:
            self.__browser.get(self.endereco_parser)

        except:
            print(warning_head_message)
            print('Não foi possível acessar o site' + self.endereco_parser)
            sys.exit('verifique o ocorrido e reinicie esta aplicação. Execeução da aplicação abortada')

    @property
    def xml_lista_total(self):
        return self.__xml_lista_total

    @property
    def insucesso(self):
        # @todo verificar
        return self.__insucesso

    @property
    def result_directory(self):
        return self.__result_directory

    @result_directory.setter
    def result_directory(self, var):
        self.__result_directory = var
        # >>> print([x[0] for x in os.walk('.')])

    def __remove_head(self, f: str) -> dict:
        """
        Remove os cabecalhos indesejados dos arquivos .htm , identifica o tipo de norma, e o numero ( na epigrafe ) e
        devolve um dict

        :param f: str com o conteudo o arquivo .htm
        :return: dict ->         resultado = {'numero':'', 'ano': '' , 'conteudo': 'texto htm limpo', 'epigrafe':'texto da epigrafe'}
        """
        ano: str = ''
        numero: str = ''
        epigrafe: str = ''
        resultado_htm: list = []
        f = f.replace("\n"," ")
        f = f.replace(">", ">\n")
        file_list = f.split('\n')
        pos_body = False
        pos_epigrafe = False
        pos_numero = False
        pos_ano = False
        is_excerto_epigrafe = False

        for idx, line in enumerate(file_list):

            # ser for um title da página exclui
            if re.search("<?title>", line) and not pos_body:
                continue

            # verifica se ja entrou no body
            if re.search("<body", line) and not pos_body:
                pos_body = True
                resultado_htm.append(line)
                continue

            # checa se é padrão epigrafe
            padrao_epigrafe = self.__tipo_norma # + '.*?<'
            excerto: str = line.lower()
            excerto = excerto.replace('.', '').replace('<.*?>', '')
            excerto = re.sub('<.*?>', '', excerto)
            excerto = re.sub('.*?>', '', excerto)
            #print('excerto',excerto)
            excerto_epigrafe = re.search(padrao_epigrafe, line.lower())
            if excerto_epigrafe:
                is_excerto_epigrafe = True
            if pos_body and is_excerto_epigrafe:
                if not pos_numero:
                    teste_numero = re.search(r"[0-9]+", excerto)
                    if teste_numero:
                        numero = teste_numero.group(0)
                        print('numero', numero)
                        pos_numero = True

            if pos_body and (not pos_ano and pos_numero):
                #print('excerto em pos_body and (not pos_ano and pos_numero)', excerto)
                teste_dia = re.search('[0-9]{1,2}º? ', excerto)
                if teste_dia:
                    dia: str = teste_dia.group(0).strip()
                    dia = dia.replace('º','')
                    #print ('dia',dia)

                for mes_referencia in self.meses:
                    teste_mes_extenso = re.search(mes_referencia, excerto)
                    if teste_mes_extenso:
                        mes: str = teste_mes_extenso.group(0)

                teste_ano = re.search('[21]{1}[890]{1}[0-9]{2}', excerto)
                if teste_ano:
                    ano = teste_ano.group(0)
                    pos_ano = True

            if pos_numero and pos_ano:
                if not pos_epigrafe:
                    #print(self.__tipo_norma.upper())
                    #print(numero)
                    #print(dia)
                    #print(mes)
                    #print(ano)
                    epigrafe = self.__tipo_norma.upper() + ' Nº ' + numero + ', ' + 'DE ' + dia + ' DE ' + mes.upper() + ' DE ' + ano
                    line = epigrafe
                is_excerto_epigrafe = False
                pos_epigrafe = True

            # depois do <boddy> desconsiderar tudo até a epigrafe
            if pos_body and not pos_epigrafe:
                continue

            # else - ou seja em qualquer outro caso nao analisado acima, monte
            resultado_htm.append(line)

        resultado_htm_concatenado = "\n".join(resultado_htm)
        return {'numero': numero, 'ano': ano, 'conteudo': resultado_htm_concatenado, 'epigrafe': epigrafe}

    def __include_epigrafe(self, xml_str: str, epigrafe: str) -> str:
        """
        Inclui o conteudo da variavel epigrafe na tag <Epigrafe> do xml constante na variável xml_str

        :param xml_str: string contendo o  XML convertido a partir do arquivo
        :param epigrafe: string com o texto que sera implantado na epigrafe
        :return: str contendo o xml com a epigrafe corrigida
        """
        print('...incluindo epigrafe ajustada')
        root = etree.XML(xml_str)  #fromstring(xml_str)   # ET.fromstring(xml_str)
        # xlm_epigrafe = root.findall('.//Epigrafe')
        root[2][0][0][0].text = epigrafe
        # print('xlm_epigrafe', xlm_epigrafe)
        print('...epigrafe ajustada incluída')
        return etree.tostring(root, encoding='unicode') #'ISO-8859-1'

    def __save_xml(self, xml_com_epigrafe: str) -> bool:
        """
        Salva a string em html

        :param xml_com_epigrafe: string com a epigrafe já corrigida
        :return: bool
        """
        print('... salvando lexml')
        if not os.path.isdir('lexml'):
            os.mkdir('lexml')

        filename = './lexml/' + self.__file + '.xml'
        self.__xml_lista_total.append(xml_com_epigrafe)
        print('conteudo do xml transformado')
        #print(xml_com_epigrafe)
        #getpass('Pressione enter para continuar')
        try:
            with open(filename, 'w') as f:
                f.write(xml_com_epigrafe)
            f.close()
        except:
            print('Não foi possível salvar o arquivo!')
            os.remove(filename)
            self.__insucesso.append(filename)

            return False
        else:
            if os.stat(filename).st_size <= 300:
                return False
            else:
                return True

    def convert_string_docx(self, file_no_head: str) -> bool:
        """
        converte a string fornecida em um docx retornando um dicionário, retornando True ou False, conforme o resultado

        :param file_no_head: str do html tratado, limpando o cabeçalho ( Casa Civil , etc...
        :return: True or False
        """
        try:
            pypandoc.convert(source=file_no_head, format='html', to='docx', outputfile=self.arquivo_docx_intermediario)
        except:
            return False
        else:
            return True

    def convert_docx_xml(self, file_no_head: dict, epigrafe: str, norma_legal = True) -> bool:
        """
        Se utiliza da instância disponivel em:
         https://legis.senado.gov.br/lexml-parser/parse/static/simulador/simulador.html

        para converter  o arquivo output.docx em .xml

        :param file_no_head: {'numero': numero, 'ano': ano, 'conteudo': resultado_htm_concatenado}
        :return: bool
        """
        try:
            self.__browser.refresh()
        except:
            return False

        if norma_legal:
            tipo_norma_tipo = self.normas_tipos[self.__tipo_norma]
        else:
            tipo_norma_tipo = 'decreto'

        autoridade = Select(self.__browser.find_element_by_id("comboAutoridade"))
        autoridade.select_by_value('federal')

        tipo_documento = Select(self.__browser.find_element_by_id("comboTipoNorma"))
        tipo_documento.select_by_value(tipo_norma_tipo)

        autoridade.select_by_value('federal')

        informar_documento = self.__browser.find_element_by_id('buttonFormGrupoInformatIdent')
        informar_documento.click()
        time.sleep(1)
        numero = self.__browser.find_element_by_id("editNumero")
        numero.clear()
        numero.send_keys(file_no_head['numero'])

        ano = self.__browser.find_element_by_id("editAno")

        numero.click()
        ano.send_keys(int(file_no_head['ano']))
        time.sleep(1)
        ano.send_keys(int(file_no_head['ano']))

        file = self.__browser.find_element_by_id('inputFile')
        pathfile = str(os.path.abspath(os.getcwd()))
        pathfile = pathfile + "/" + self.arquivo_docx_intermediario

        #print('arquivo_docx_inermediario....:', pathfile)
        file.send_keys(pathfile)
        file.submit()

        try:
            xml = WebDriverWait(self.__browser, 30).until(
                EC.visibility_of_element_located((By.XPATH, "//div[@id='grupoSaidas']/a[1]"))
            )
            link_exemplo = xml.get_attribute('href')
            link_exemplo = re.findall("result/.*?/", link_exemplo)
            link_exemplo = re.sub("result/", "", link_exemplo[0])
            link_exemplo = re.sub("/", "", link_exemplo)
        except:
            return False
        else:
            href_antes = 'http://legis.senado.gov.br/lexml-parser/parse/result/'
            href_depois = '/gerado/documento.xml'
            href = href_antes + link_exemplo + href_depois

            #browser2 = webdriver.Firefox()
            #@todo tratar aqui, se nao conseguiu fazer o parser tem que pular para o proximo
            try:
                self.__browser2.get(href)
            except:
                return False

            xml_com_epigrafe = self.__include_epigrafe(self.__browser2.page_source, epigrafe)

            return self.__save_xml(xml_com_epigrafe)

    def fit_files(self, directory=".", close_browser=True) -> bool:
        """
        Itera com todos os arquivos .htm  do dirctory apontado no parametero  e faz a conversoã para .xml lexml.

        :param directory: diretório onde se encontram os arquivos a serem parseados
        :param save_file: bool
        :return: bool
        """
        file = ''
        counter_sucess: int = 0
        for file in os.listdir(directory):
            if file.endswith(".htm"):
                self.__file = file
                print("////////////////// PARSEANDO ARQUIVO //////////////////////////////")
                print("tratando o arquivo ", file)
                filepath = directory + '/' + file
                with open(filepath, encoding='latin1', errors='replace') as f: #@todo ou seria melhor utf8?
                    # remove o cabeçalho da norma
                    file_no_head: dict = self.__remove_head(f.read())
                    resultado = self.convert_string_docx(file_no_head['conteudo'])
                    if not resultado:
                        continue
                    conversao: bool = self.convert_docx_xml(file_no_head, file_no_head['epigrafe'])
                    if conversao:
                        counter_sucess += 1
                        print('...arquivo ' + file + ' convertido com sucesso')
                    else:
                        self.__insucesso.append(file)
                        print('não foi possível converter o arquivo' + file)
                    print("... apagando o .docx")
                    os.remove(self.arquivo_docx_intermediario)

                    print(
                        """
                            Fim do parseamento,
                            
                        """
                    )

                    print('diretório com os arquivos xml: ', self.__result_directory)
                    # @todo verificar o resul_directory
                    print('quantidade de arquivos parseados com sucesso: ', counter_sucess)
                    # converter para docx
        print('Arquivo' + file + ' parseado!')
        if close_browser:
            self.__browser.close()
            self.__browser2.close()
        return True
    def fit_directories_tree(self, list_directories: str, save_file=True) -> bool:
        """
        Por base na lista de diretórios disponíveis em um arquivo .csv ( que cotenha coluna única com o endereço dos
        diretórios), trata cada arquivo de cada diretório listado.

        :param list_directories:  nome do arquivo .csv
        :param save_file: caso negativo, será  respondida apenas uma lista contendo os xmls
        :return: bool
        """

        with open(list_directories) as csv_file:
            reader = csv.reader(csv_file, delimiter=',')
            rows = list(reader)
            for diretorio in rows:
                self.fit_files(save_file, diretorio[0], False)
        return False
